# Disclaimer

This GitLab library is deprecated. Please, refer to the GitHub library https://github.com/thijor/pyntbci with documentation on https://thijor.github.io/pyntbci/.

# Contact

* Jordy Thielen (jordy.thielen@donders.ru.nl)
